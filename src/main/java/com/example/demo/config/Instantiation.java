package com.example.demo.config;

import com.example.demo.domain.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class Instantiation implements CommandLineRunner {

    @Autowired
    private UserRepository userRepository;

    @Override
    public void run(String... args) throws Exception {

        userRepository.deleteAll();

        List<User> userList = new ArrayList<>();

        userList.add(new User(null, "Breno Martins do Nascimento", "brenomartins@ciandt.com"));
        userList.add(new User(null, "Caique Gonçalves da Silva", "caiquegs@ciandt.com"));
        userList.add(new User(null, "Estêvão Francisco Fáy", "estevao@ciandt.com"));
        userList.add(new User(null, "Fabio Flaitt", "fabiofj@ciand.com"));
        userList.add(new User(null, "Felipe Henrique da Silva", "felipehs@ciandt.com"));
        userList.add(new User(null, "Felipe Manzoli Ramos", "felipehs@ciandt.com"));
        userList.add(new User(null, "Filipe Figueira Souza Zanelato", "fzanelato@ciandt.com"));
        userList.add(new User(null, "Gabriel Talles Lopes", "gtalles@ciandt.com"));
        userList.add(new User(null, "Henrique Verrengia de Oliveira", "henriquev@ciandt.com"));
        userList.add(new User(null, "Jeriel Verissimo", "jeriel@ciandt.com"));
        userList.add(new User(null, "Pablo Borsone", "pborsone@ciandt.com"));
        userList.add(new User(null, "Paulo Jorge Rodrigues Benjamim Ramos", "paulobr@ciandt.com"));
        userList.add(new User(null, "Paulo Vinicius Martimiano de Oliveira", "paulovm@ciadnt.com"));
        userList.add(new User(null, "Rodrigo de Oliveira Soares", "rodrigosoares@ciandt.com"));
        userList.add(new User(null, "Ronaldo Rodrigues Fernandes Vieira", "ronaldofv@ciandt.com"));
        userList.add(new User(null, "Tiago Fernandes Tasselli", "tiagotasseli@ciandt.com"));
        userList.add(new User(null, "Victor Matos", "vitorfm@ciandt.com"));
        userList.add(new User(null, "Vinicius Silva Machado", "viniciussm@ciandt.com"));

        userRepository.saveAll(userList);

    }
}
